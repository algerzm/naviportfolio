import axios from 'axios'

const dev = 'http://localhost:5000/v1'
const prod = 'https://emailer.algerzm.me/v1'
const pcnavi = 'http://192.168.3.27:4000/'

const axiosClient = axios.create({
  baseURL: prod,
})

axiosClient.interceptors.request.use((config) => {
  if (config.method === 'get') {
    console.log('Doing get')
  }
  return config
})

axiosClient.interceptors.response.use(
  (res) => {
    console.log('Response del succes')
    return res
  },
  (error) => {
    console.log(error.response)
    return Promise.reject(error.response.data.message)
  }
)

export default axiosClient
