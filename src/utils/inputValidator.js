import { validate } from 'email-validator'

const validateEmail = (email) => validate(email)

const validateContent = (content) => {
  if (content.length >= 20) {
    return true
  }

  return false
}

const validateName = (name) => {
  if (name.length >= 10) {
    return true
  }

  return false
}

export { validateEmail, validateContent, validateName }
