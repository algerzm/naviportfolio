import styled from 'styled-components'
import { Grid, Typography, Button } from '@mui/material'

import ContactForm from './ContactForm'

const ContactContainer = styled('div')`
  ${({ theme }) => `
    padding: 5;
    width: 100vw;
    padding-bottom: 5vh;
    background: white;
  `}
`

const ContactTitle = styled(Typography)`
  ${({ theme }) => `
  padding: 2rem;
  font-weight: bold;
  background-color: #0f0f0f;
  color: transparent;
  text-shadow: 0px 2px 3px #4747477c;
  -webkit-background-clip: text;
     -moz-background-clip: text;
          background-clip: text;
`}
`

const ContactInfoGrid = styled(Grid)`
  ${({ theme }) => `
    padding-left: 5vw;
    @media (min-width: 2000px) {
      padding-left: 15vw;
      padding-right: 0vw;
    }
    @media (max-width: 767px) {
      padding-right: 5vw;
    }
  `}
`

export default function ContactSection() {
  return (
    <ContactContainer>
      <ContactTitle align="center" variant="h3">
        Let's get in touch!
      </ContactTitle>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          marginTop: '-25px',
        }}
      >
        <div
          style={{
            width: '90vw',
            height: '7px',
            backgroundColor: '#00325e',
            marginBottom: '5vh',
          }}
        ></div>
      </div>
      <Grid container>
        <ContactInfoGrid xs={12} sm={4} item>
          <p>
            Feel Free to contact me! You can leave your message here and i'll
            answer it as soon as possible.
          </p>
          <p>Also you can get in touch with me through my social media.</p>
          <div
            style={{
              width: '100%',
              display: 'flex',
              justifyContent: 'space-evenly',
              marginTop: '2rem',
              marginBottom: '1rem',
            }}
          >
            <Button
              href="https://www.linkedin.com/in/alger-ivan-zamudio-monta%C3%B1o-bb82521b0"
              target="_blank"
              sx={{ width: '55px', height: '55px' }}
            >
              <img
                style={{ width: '60px', height: '60px' }}
                src="https://iconape.com/wp-content/files/yd/367773/svg/logo-linkedin-logo-icon-png-svg.png"
                alt=""
              />
            </Button>
            <Button
              sx={{ width: '55px', height: '55px' }}
              href="https://gitlab.com/algerzm"
              target="_blank"
            >
              <img
                style={{ width: '77px', height: '77px' }}
                src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png"
                alt=""
              />
            </Button>
            <Button
              sx={{ width: '55px', height: '55px' }}
              href="https://github.com/algerivan"
              target="_blank"
            >
              <img
                style={{ width: '48px', height: '48px' }}
                src="https://cdn-icons-png.flaticon.com/512/25/25231.png"
                alt=""
              />
            </Button>
            <Button sx={{ width: '55px', height: '55px' }}>
              <img
                style={{ width: '58px', height: '58px' }}
                src="https://deportesinc.com/depsite/wp-content/uploads/2017/10/60414c58e954d7236837248225e0216f_new-twitter-logo-vector-eps-twitter-logo-clipart-png_518-518.png"
                alt=""
              />
            </Button>
          </div>
        </ContactInfoGrid>
        <Grid xs={12} sm={8} item>
          <ContactForm />
        </Grid>
      </Grid>
    </ContactContainer>
  )
}
