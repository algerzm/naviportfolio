import { useState, useEffect } from 'react'
import { TextField, Grid, Button } from '@mui/material'
import Swal from 'sweetalert2'
import axiosClient from '../../api/axiosClient'
import styled from 'styled-components'
import {
  validateEmail,
  validateContent,
  validateName,
} from '../../utils/inputValidator'
import '../sweetAlertClasses.css'

const ContactFormGrid = styled(Grid)`
  ${({ theme }) => `
    padding-right: 5vw;
    padding-left: 5vw;
    @media (min-width: 2000px) {
      padding-left: 5vw;
      padding-right: 15vw;
    }
  `}
`

export default function ContactForm() {
  const [formValues, setFormValues] = useState({
    name: '',
    email: '',
    content: '',
  })

  const [errors, setErrors] = useState({
    name: false,
    email: false,
    content: false,
  })

  const handleChange = (e) => {
    setFormValues({
      ...formValues,
      [e.target.name]: e.target.value,
    })
  }

  const handleSubmit = async () => {
    let inputErrors = { ...errors }

    validateEmail(formValues.email)
      ? (inputErrors = { ...inputErrors, email: false })
      : (inputErrors = { ...inputErrors, email: true })

    validateContent(formValues.content)
      ? (inputErrors = { ...inputErrors, content: false })
      : (inputErrors = { ...inputErrors, content: true })

    validateName(formValues.name)
      ? (inputErrors = { ...inputErrors, name: false })
      : (inputErrors = { ...inputErrors, name: true })

    setErrors({ ...inputErrors })
  }

  useEffect(() => {
    const sendEmail = () => {
      axiosClient
        .post('/text-email', {
          to: 'algerzm@gmail.com',
          subject: `PORTAFOLIO: ${formValues.email}`,
          email: formValues.email,
          name: formValues.name,
          content: formValues.content,
        })
        .then((res) => {
          Swal.fire({
            title: 'Success!',
            text: 'Your e-mail was sent successfully! :D',
            icon: 'success',
            confirmButtonText: 'Great!',
            customClass: {
              confirmButton: 'buttonColor',
            },
          })
        })
        .catch((err) => {
          Swal.fire({
            title: 'Error :c',
            text: 'Something went wrong sending your email, try again later.',
            icon: 'error',
            confirmButtonText: 'Okay',
            customClass: {
              confirmButton: 'buttonColor',
            },
          })
        })
    }

    if (!errors.name && !errors.email && !errors.content) {
      if (
        formValues.email !== '' &&
        formValues.name !== '' &&
        formValues.content !== ''
      ) {
        sendEmail()
      }
    }
  }, [errors]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <ContactFormGrid component="form" noValidate autoComplete="off" container>
      <Grid item sx={{ padding: 3, marginTop: '-15px' }} xs={12} sm={6}>
        <TextField
          id="outlined-error"
          name="name"
          error={errors.name}
          helperText={errors.name ? 'Enter a valid name.' : ''}
          label="Full Name"
          placeholder="What's your name?"
          fullWidth
          onChange={handleChange}
        />
      </Grid>
      <Grid item sx={{ padding: 3, marginTop: '-15px' }} xs={12} sm={6}>
        <TextField
          id="outlined-error"
          error={errors.email}
          helperText={errors.email ? 'Enter a valid email address.' : ''}
          name="email"
          label="Email"
          placeholder="Write your email ;)"
          fullWidth
          onChange={handleChange}
        />
      </Grid>
      <Grid item sx={{ padding: 3, marginTop: '-15px' }} xs={12}>
        <TextField
          id="outlined-multiline-static"
          error={errors.content}
          helperText={
            errors.content
              ? 'Verify your message please (Min 25 characters).'
              : ''
          }
          label="Content"
          name="content"
          multiline
          rows={4}
          placeholder="Your message here! :D"
          fullWidth
          onChange={handleChange}
        />
      </Grid>
      <Grid item sx={{ padding: 3, marginTop: '-25px' }} xs={12}>
        <Button
          onClick={() => handleSubmit()}
          color="primary"
          variant="contained"
        >
          Contact Me!
        </Button>
      </Grid>
    </ContactFormGrid>
  )
}
