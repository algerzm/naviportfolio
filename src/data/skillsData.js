const skills = [
  {
    name: 'HTML5',
    image: 'https://navizm.com/images/skills/html5.png',
  },
  {
    name: 'CSS3',
    image: 'https://navizm.com/images/skills/css3.png',
  },
  {
    name: 'JavaScript',
    image: 'https://navizm.com/images/skills/javascript.png',
  },
  {
    name: 'React JS',
    image: 'https://navizm.com/images/skills/react.png',
  },
  {
    name: 'Vue JS',
    image: 'https://navizm.com/images/skills/vue.png',
  },
  {
    name: 'Node JS',
    image: 'https://navizm.com/images/skills/nodejs.png',
  },
  {
    name: 'Express JS',
    image: 'https://navizm.com/images/skills/express.png',
  },
  {
    name: 'C#',
    image: 'https://navizm.com/images/skills/csharp.png',
  },
  {
    name: '.NET Core',
    image: 'https://navizm.com/images/skills/netcore.png',
  },
  {
    name: 'PHP',
    image: 'https://navizm.com/images/skills/php.png',
  },
  {
    name: 'PostgreSQL',
    image: 'https://navizm.com/images/skills/postgresql.png',
  },
  {
    name: 'SQL Server',
    image: 'https://navizm.com/images/skills/sqlserver.png',
  },
  {
    name: 'Mongo DB',
    image: 'https://navizm.com/images/skills/mongodb.png',
  },
  {
    name: 'REST API',
    image: 'https://navizm.com/images/skills/restapi.png',
  },
  {
    name: 'JSON',
    image: 'https://navizm.com/images/skills/json.png',
  },
  {
    name: 'Docker',
    image: 'https://navizm.com/images/skills/docker.png',
  },
  {
    name: 'NGINX',
    image: 'https://navizm.com/images/skills/nginx.png',
  },
  {
    name: 'GIT',
    image: 'https://git-scm.com/images/logos/downloads/Git-Icon-Black.png',
  },
]

export { skills }
